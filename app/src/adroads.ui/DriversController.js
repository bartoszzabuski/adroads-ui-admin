(function () {

    angular
        .module('users').controller('driversController', function ($scope, driverHttpService, $mdDialog, $location) {
        var self = this;

        self.filterCriteriaResource = {
            sortDir: 'ASC',
            sortedBy: '',
            carBody: []
        };

        self.constants = {
            carBodies: ["hatchback", "kabriolet", "kombi", "mpv", "pickup", "sedan", "limuzyna", "coupe", "SUV", "terenowy", "van"]
        };

        self.driverStatuses = [
            {
                value: "",
                label: ""
            },
            {
                value: "CREATED",
                label: "stworzony"
            }, {
                value: "REGISTERED",
                label: "zarejestrowany"
            }, {
                value: "TAKEN",
                label: "w kampanii"
            }
        ];

        self.pageSize = 10;
        self.currentLoadedPage = 1;
        self.totalNumberOfItems = 1003;

        self.toggle = toggleCheckbox;
        self.filterResults = filterResults;
        self.loadMoreResults = loadMoreResults;
        self.openAddDriverToCampaignDialog = openAddDriverToCampaignDialog;
        self.addDriverToCampaign = addDriverToCampaign;
        self.goToEditDriver = goToEditDriver;

        function goToEditDriver(driverId) {
            $location.path('/editDriver/' + driverId);
        }

        // UI
        function toggleCheckbox(item, list) {
            var idx = list.indexOf(item);
            if (idx > -1) list.splice(idx, 1);
            else list.push(item);
        };

        //backend calls
        function filterResults() {
            self.currentLoadedPage = 1;
            console.log("about to load first page....");
            self.filterCriteriaResource = removeAllBlankOrNull(self.filterCriteriaResource);
            console.log(JSON.stringify(self.filterCriteriaResource));
            console.log($.param(self.filterCriteriaResource));
            driverHttpService.loadDrivers(self.filterCriteriaResource, self.pageSize).then(function (response) {
                console.log("number of results " + response.data.drivers.length);
                setUpPage(response.data);
            });
        }

        function loadMoreResults() {
            console.log("about to load more....");
            self.filterCriteriaResource = removeAllBlankOrNull(self.filterCriteriaResource);
            console.log(JSON.stringify(self.filterCriteriaResource));
            console.log($.param(self.filterCriteriaResource));
            driverHttpService.loadMoreDrivers(self.filterCriteriaResource, self.pageSize, getUuidBy(++self.currentLoadedPage)).then(function (response) {
                console.log("number of results " + response.data.drivers.length);
                setUpPage(response.data);
            });
        }

        // utils
        function getUuidBy(pageNumber) {
            console.log(pageNumber);
            var metadata = JSON.parse($scope.metadata);
            console.log(metadata);
            console.log(metadata.pageMetadataList);

            var found = metadata.pageMetadataList.filter(
                function (metadata) {
                    return metadata.pageNumber == pageNumber
                }
            );
            console.log("found: " + found);
            return found[0].uuid;
        }

        //// TODO debug purposes -> remove once unwanted
        //$scope.printResults = function () {
        //    self.filterCriteriaResource = removeAllBlankOrNull(self.filterCriteriaResource);
        //    console.log(JSON.stringify(self.filterCriteriaResource));
        //};

        function setUpPage(response) {
            console.log("inside set up page " + JSON.stringify(response.metadata));
            if (response.metadata != null) {
                $scope.metadata = JSON.stringify(response.metadata, undefined, 2);
                $scope.drivers = response.drivers;
            } else {
                $scope.drivers.extend(response.drivers);
            }
        }

        function removeAllBlankOrNull(JsonObj) {
            $.each(JsonObj, function (key, value) {
                if (value === "" || value === null) {
                    delete JsonObj[key];
                } else if (typeof(value) === "object") {
                    JsonObj[key] = removeAllBlankOrNull(value);
                }
            });
            return JsonObj;
        }

        function openAddDriverToCampaignDialog($event, driversId) {
            console.log($scope);
            $mdDialog.show({
                controller: AddDriverToCampaignModalController,
                controllerAs: 'ctrl',
                templateUrl: 'src/adroads.ui/view/dialogs/addDriverToCampaignController.tmpl.html',
                parent: angular.element(document.body),
                targetEvent: $event,
                clickOutsideToClose: true,
                scope: $scope,
                locals: {
                    driversId: driversId
                }
            })
        }

        function addDriverToCampaign(campaignId, driverId, $event) {
            driverHttpService.addDriverToCampaign(campaignId, driverId).then(function (response) {
                $mdDialog.show(
                    $mdDialog.alert()
                        .clickOutsideToClose(true)
                        .title('Success')
                        .textContent('Kierowca dodany')
                        .targetEvent($event)
                );
            });
        }

        function AddDriverToCampaignModalController($timeout, $q, targetEvent, $mdDialog, driverHttpService, driversId) {
            var self = this;

            self.driversId = driversId;
            self.selectedItem = {};
            self.driversController = $scope.controller;

            self.campaignNames = loadAll();
            self.querySearch = querySearch;
            self.add = add;

            //TODO fix it. It's not good to be working with DOM elements in controllers!!
            self.button = (targetEvent.target.type == 'button') ? targetEvent.target : targetEvent.target.parentElement;

            self.cancel = function ($event) {
                $mdDialog.cancel();
            };

            self.hide = function ($event) {
                $mdDialog.hide();
            }

            function add($event) {
                self.driversController.addDriverToCampaign(self.selectedItem.value, driversId, $event);
                self.hide($event);
                self.button.disabled = true;
            }

            function querySearch(query) {
                return query ? self.campaignNames.filter(createFilterFor(query)) : self.campaignNames;
            }

            function loadAll() {
                //TODO maybe that should be called from parent controller and the pass promise here?
                return driverHttpService.fetchCampaignNames().then(function (response) {
                    console.log("number of results " + response.data.length);
                    console.log("results " + response.data);
                    self.campaignNames = response.data.map(function (campaign) {
                        return {
                            value: campaign.id,
                            display: campaign.name
                        };
                    });
                });
            }

            function createFilterFor(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(campaign) {
                    return (campaign.value.indexOf(lowercaseQuery) === 0);
                };
            }
        }

        // TODO where to move this bad boy?
        Array.prototype.extend = function (other_array) {
            /* you should include a test to check whether other_array really is an array */
            other_array.forEach(function (v) {
                this.push(v)
            }, this);
        }

    });

})();
