(function () {

    angular
        .module('users').controller('EditDriverController', function ($scope, $stateParams, driverHttpService, $mdDialog, $filter) {
        var self = this;

        $scope.driver = {};

        self.init = init;
        self.saveChanges = saveChanges;

        function init() {
            $scope.driver = driverHttpService.loadDriver($stateParams.driverId).then(function (response) {
                $scope.driver = response.data;

                console.log($scope.driver);

                $scope.dateOfBirth = parseDate($scope.driver.dateOfBirth);
                //$scope.selectedProvince = $scope.driver.address.province;
                //$scope.selectedCarMake = $scope.driver.carDetails.make;
            });
        }


        // TODO extract -> same bit in EditDriverController
        function formatDateOfBirth(date) {
            return $filter('date')(date, "dd-MM-yyyy");
        }

        function saveChanges() {
            $scope.driver.dateOfBirth = formatDateOfBirth($scope.dateOfBirth);
            console.log(JSON.stringify($scope.driver));
            driverHttpService.updateDriver($scope.driver, $scope.driver.uuid)
                .then(function (response) {
                    if (response.status == '200') {
                        $mdDialog.show(
                            $mdDialog.alert()
                                //.parent(angular.element(document.querySelector('#popupContainer')))
                                .clickOutsideToClose(true)
                                .title('Info')
                                .textContent('Kierowca został zapisany')
                                .ok('ok!')
                            //.targetEvent(ev)
                        );
                    } else {
                        console.log(response);
                    }
                });
        }

        function parseDate(dateString) {
            var dateChunks = dateString.split("-");
            return new Date(dateChunks[0], dateChunks[1] - 1, dateChunks[2]);
        }

        // TODO extract from here and from registration controller
        // constants
        $scope.carMakes = [
            'Alfa Romeo',
            'Audi',
            'BMW',
            'Cadillac',
            'Chevrolet',
            'Chrysler',
            'Citroen',
            'Dacia',
            'Daewoo-FSO',
            'Daihatsu',
            'Dodge',
            'Fiat',
            'Ford',
            'Honda',
            'Hyundai',
            'Infiniti',
            'Jaguar',
            'Jeep',
            'Kia',
            'Lancia',
            'Land Rover',
            'Lexus',
            'Łada',
            'Mazda',
            'Mercedes-Benz',
            'MG',
            'Mini',
            'Mitsubishi',
            'Nissan',
            'Opel',
            'Peugeot',
            'Renault',
            'Saab',
            'Seat',
            'Skoda',
            'Smart',
            'SsangYong',
            'Subaru',
            'Suzuki',
            'Toyota',
            'Volkswagen',
            'Volvo',
            'inny'
        ];

        $scope.carYears = [
            '2015',
            '2014',
            '2013',
            '2012',
            '2011',
            '2010',
            '2009',
            '2008',
            '2007',
            '2006',
            '2005',
            '2004',
            '2003',
            '2002',
            '2001',
            '2000',
            '1999',
            '1998',
            '1997',
            '1996',
            '1995',
            '1994',
            '1993',
            '1992',
            '1991',
            '1990',
            '1989',
            '1988',
            '1987',
            '1986',
            '1985',
            '1984',
            '1983',
            '1982',
            '1981',
            '1980',
            'starsze'
        ];

        $scope.carBodies = [
            'hatchback',
            'kabriolet',
            'kombi',
            'mpv',
            'pickup',
            'sedan',
            'limuzyna',
            'coupe',
            'suv',
            'terenowy',
            'van'
        ];

        $scope.carColours = [
            'Beżowy',
            'Biały',
            'Bordowy',
            'Brązowy',
            'Czarny',
            'Czerwony',
            'Fioletowy',
            'Niebieski',
            'Srebrny',
            'Szary',
            'Zielony',
            'Żółty',
            'inny'
        ];

        $scope.provinces = [
            'dolnośląskie',
            'kujawsko-pomorskie',
            'lubelskie',
            'lubuskie',
            'łódzkie',
            'małopolskie',
            'mazowieckie',
            'opolskie',
            'podkarpackie',
            'podlaskie',
            'pomorskie',
            'śląskie',
            'świętokrzyskie',
            'warmińsko-mazurskie',
            'wielkopolskie',
            'zachodniopomorskie'
        ];

        $scope.driversTypes = [
            {label: 'Przedstawiciel/handlowiec', value: '1'},
            {label: 'Kurier/Transport', value: '2'},
            {label: 'Taksówkarz', value: '3'},
            {label: 'Dojazd z/do pracy', value: '4'},
            {label: 'inny', value: '5'}
        ];
    });

})();
