(function () {

    angular
        .module('users').controller('CampaignDetailsController', function ($scope, $stateParams, driverHttpService, $mdDialog) {
        var self = this;

        self.campaign = {};

        self.init = init;

        function init() {
            self.campaign = driverHttpService.fetchCampaign($stateParams.campaignId).then(function (response) {
                self.campaign = response.data;
            });
        }
    });

})();
