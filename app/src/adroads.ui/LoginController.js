(function () {

    angular
        .module('users').controller('LoginController', function (AuthenticationService, LoggedInUserService, $state) {
        var self = this;

        self.loginRequest = {};

        self.login = login;

        function login() {
            console.log(self.loginRequest.login);
            console.log(self.loginRequest.password);

            AuthenticationService.authenticate(self.loginRequest.login, self.loginRequest.password)
                .success(function (data) {
                    console.log("storing token: " + data.token);
                    LoggedInUserService.setLoggedIn(self.loginRequest.login, data.token);
                    $state.go('loggedIn');
                })
                .error(function (data){
                    console.log('errroerrrrr');
                });
        }

    });

})();
