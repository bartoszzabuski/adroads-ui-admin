// Prepare the 'main' module for subsequent registration of controllers and delegates
angular.module('users', ['ngMaterial', 'ui.router', 'satellizer', 'ngStorage', 'ngMessages'])

    .config(function ($mdThemingProvider) {
        $mdThemingProvider.theme("default").primaryPalette("cyan").accentPalette("light-green");
    })
    .config(["$stateProvider", "$urlRouterProvider", "$authProvider", "$httpProvider",
        function ($stateProvider, $urlRouterProvider, $authProvider, $httpProvider) {

            //$authProvider.loginUrl = 'http://localhost:8080/users/login';

            $httpProvider.interceptors.push('HttpErrorHandler');
            $httpProvider.interceptors.push('HttpInterceptor');

            $urlRouterProvider.otherwise("/login");

            $stateProvider
                .state('campaignDetails', {
                    url: '/campaignDetails/:campaignId',
                    templateUrl: 'src/adroads.ui/view/campaignDetails.html',
                    controller: "CampaignDetailsController"
                })
                .state('editDriver', {
                    url: '/editDriver/:driverId',
                    templateUrl: 'src/adroads.ui/view/editDriver.html',
                    controller: "EditDriverController"
                })
                .state('campaigns', {
                    url: "/campaigns",
                    templateUrl: "src/adroads.ui/view/campaigns.html",
                    controller: "campaignsController"
                })
                .state('campaigns-calendar', {
                    url: '/campaigns-calendar',
                    templateUrl: "src/adroads.ui/view/campaigns-calendar.html",
                    controller: "CampaignsCalendarController"
                })
                .state('drivers', {
                    url: '/drivers',
                    templateUrl: "src/adroads.ui/view/drivers.html",
                    controller: "driversController"
                })
                .state('register-driver', {
                    url: '/register-driver',
                    templateUrl: "src/adroads.ui/view/drivers-registration.html",
                    controller: "DriversRegistrationController"
                })
                .state('login', {
                    url: '/login',
                    templateUrl: "src/adroads.ui/view/login.html",
                    controller: "LoginController"
                })
                .state('loggedIn', {
                    url: '/loggedIn',
                    templateUrl: "src/adroads.ui/view/loggedIn.html"
                    //controller: "LoggedInController"
                });
        }])
    .constant("urls", {
        "driversEndpoint": "https://localhost:7878/drivers/",
        "campaignsEndpoint": "https://localhost:7878/campaigns/",
        "queryEndpoint": "https://localhost:7878/query/drivers/",
        "usersEndpoint": "https://localhost:7878/users/",
        "campaignQueryEndpoint": "https://localhost:7878/query/campaigns",
        "campaignNamesQueryEndpoint": "https://localhost:7878/query/campaign-indexes",
        "webBackendAddress": "https://localhost/adroads-ui-web-backend/api/address/"
    })
    .factory('driverHttpService', function ($http, urls) {
        return {
            registerDriver: function (createDriverRequest) {
                return $http.post(urls.driversEndpoint, createDriverRequest);
            },

            updateDriver: function (driver, driverId) {
                return $http.put(urls.driversEndpoint + driverId, driver);
            },

            loadDriver: function (id) {
                return $http.get(urls.queryEndpoint + id);
            },

            loadFirstDriversPage: function (pageNumber, pageSize) {
                return $http.get(urls.queryEndpoint + "?page=" + pageNumber + "&size=" + pageSize);
            },

            loadMoreDrivers: function (filterCriteriaResource, pageSize, uuid) {
                return $http.get(urls.queryEndpoint + "page" + "?filterCriteriaResource=" + JSON.stringify(filterCriteriaResource) + "&pageSize=" + pageSize + "&uuid=" + uuid);
            },

            loadDrivers: function (filterCriteriaResource, pageSize) {
                console.log(urls.queryEndpoint + "?filterCriteriaResource=" + JSON.stringify(filterCriteriaResource) + "&pageSize=" + pageSize);
                return $http.get(urls.queryEndpoint + "?filterCriteriaResource=" + JSON.stringify(filterCriteriaResource) + "&pageSize=" + pageSize);
            },
            registerCampaign: function (createCampaignRequest) {
                return $http.post(urls.campaignsEndpoint, createCampaignRequest);
            },
            addDriverToCampaign: function (campaignId, driverId) {
                return $http.put(urls.campaignsEndpoint + campaignId + "/drivers/" + driverId, {});
            },
            fetchAllCampaigns: function () {
                return $http.get(urls.campaignQueryEndpoint);
            },
            fetchCampaign: function (uuid) {
                return $http.get(urls.campaignQueryEndpoint + "/" + uuid);
            },
            fetchCampaignNames: function () {
                return $http.get(urls.campaignNamesQueryEndpoint);
            }
        }
    })
    .factory('webBackendHttpService', function ($http, urls) {
        return {
            getAddress: function (postcode) {
                return $http.get(urls.webBackendAddress + postcode);
            },
            getCarDetailsForMake: function (make) {
                return $http.get(urls.webBackendCarDetails + make);
            }
        }
    }).factory('AuthenticationService', function ($http, urls, LoggedInUserService, $state) {
        return {
            authenticate: function (login, password) {
                console.log("auth service " + login + " " + password);
                return $http.post(urls.usersEndpoint + "login", "login=" + encodeURIComponent(login) +
                    "&password=" + encodeURIComponent(password), {
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
            }
        }
    })
    .service('LoggedInUserService', function ($localStorage) {
        var self = this;
        self.loggedInUser = null;

        self.setLoggedIn = function (name, token) {
            self.loggedInUser = name;
            $localStorage.token = token;
        };

        self.logOut = function () {
            self.loggedInUser = null;
            $localStorage.token = null;
            // TODO call backend to logout user
        };

        self.clearLoggedInInfo = function () {
            self.loggedInUser = null;
            $localStorage.token = null;
        };

        self.getLoggedIn = function () {
            return self.loggedInUser;
        };

        self.getToken = function () {
            return $localStorage.token;
        };

        self.isLoggedIn = function () {
            return $localStorage.token != null || $localStorage.token != undefined;
        };

    })
    /*
        decorates http request with token
     */
    .service('HttpInterceptor', function (LoggedInUserService, $rootScope, $q) {
        var self = this;

        self.request = function (config) {
            console.log("1: " + JSON.stringify(config.headers));
            if (LoggedInUserService.isLoggedIn()) {
                var token = LoggedInUserService.getToken();
                config.headers.authorization = "Bearer " + token;
                $rootScope.$broadcast('authorized');
            }
            console.log("2: " + JSON.stringify(config.headers));
            return config;
        };

        self.responseError = function (response) {
            if (response.status == 401) {
                console.log("401 occured");
                LoggedInUserService.clearLoggedInInfo();
                $rootScope.$broadcast('unauthorized');
                //return $q.reject(response);
            }
            //return response;
            return $q.reject(response);
        }
    })
    .service('HttpErrorHandler', function ($q, $rootScope) {
        var self = this;

        self.responseSuccess = function (response) {
            return response;
        };

        self.responseError = function (response) {
            $rootScope.$broadcast('error', response.data.msg);
            return $q.reject(response);
        };
    })
    .factory('carDetailsFactory', function ($http) {
        var carsApp = 'http://polished-bird-9319.getsandbox.com';
        return {
            getMakes: function (callback) {
                $http.get(carsApp + '/makes')
                    .success(callback);
            },

            getModels: function (make, callback) {
                $http.get(carsApp + '/models/' + make)
                    .success(callback);
            },

            mockMakes: [
                "renault",
                "opel",
                "audi",
                "skoda",
                "BMW",
                "fiat"
            ],

            colours: [
                'Beżowy',
                'Biały',
                'Bordowy',
                'Brązowy',
                'Czarny',
                'Czerwony',
                'Fioletowy',
                'Niebieski',
                'Srebrny',
                'Szary',
                'Zielony',
                'Żółty',
                'inny'
            ],

            years: [
                '2015',
                '2014',
                '2013',
                '2012',
                '2011',
                '2010',
                '2009',
                '2008',
                '2007',
                '2006',
                '2005',
                '2004',
                '2003',
                '2002',
                '2001',
                '2000',
                '1999',
                '1998',
                '1997',
                '1996',
                '1995',
                '1994',
                '1993',
                '1992',
                '1991',
                '1990',
                '1989',
                '1988',
                '1987',
                '1986',
                '1985',
                '1984',
                '1983',
                '1982',
                '1981',
                '1980'
            ],

            bodies: [
                'hatchback',
                'kabriolet',
                'kombi',
                'mpv',
                'pickup',
                'sedan',
                'limuzyna',
                'sport/coupe',
                'SUV',
                'terenowy',
                'van'
            ]


        };
    })
    // Decorate the service...
    //.config(function (errorHandlerProvider, $provide) {
    //    errorHandlerProvider.decorate($provide, ['driverHttpService']);
    //})
    // The following is used to show the error messages on screen. Normally you would probably use a directive for this.
    //.run(function ($rootScope, errorHandler) {
    //    $rootScope.errorHandler = errorHandler;
    //})
    .directive('onBlurChange', function ($parse) {
        return function (scope, element, attr) {
            var fn = $parse(attr['onBlurChange']);
            var hasChanged = false;
            element.on('change', function (event) {
                hasChanged = true;
            });

            element.on('blur', function (event) {
                if (hasChanged) {
                    scope.$apply(function () {
                        fn(scope, {$event: event});
                    });
                    hasChanged = false;
                }
            });
        };
    })
    .directive('onEnterBlur', function () {
        return function (scope, element, attrs) {
            element.bind("keydown keypress", function (event) {
                if (event.which === 13) {
                    element.blur();
                    event.preventDefault();
                }
            });
        };
    });
(function () {

    'use strict';

    //TODO refactor to separate file.
    //TODO consider refactoring out the event publishing
    angular
        .module('users').controller('campaignsController', function ($scope, $location, driverHttpService, $mdDialog) {
        $scope.message = 'campaigns message';

        var self = this;

        self.campaigns = [];

        self.showAdd = showAdd;
        self.init = init;
        self.goToCampaignDetails = goToCampaignDetails;

        $scope.$on('updateCampaignsList', function () {
            self.init();
        });

        function goToCampaignDetails(campaignId) {
            $location.path('/campaignDetails/' + campaignId);
        }

        function init() {
            driverHttpService.fetchAllCampaigns()
                //.then(function (response) {
                //  var campaignId = response.headers("location").split("/").pop();
                //  console.log("response: " + response);
                //});
                .success(function (data, status, headers, config) {
                    console.log(data);
                    console.log(status);
                    console.log(headers());
                    console.log(config);

                    self.campaigns = data;
                })
                .error(function (data, status, headers, config) {
                    console.log(data);
                    console.log(status);
                    console.log(headers());
                    console.log(config);
                });
        }

        // create new campaign dialog
        function DialogController($scope, $rootScope, $mdDialog) {
            $scope.hide = function () {
                $mdDialog.hide();
            };
            $scope.cancel = function () {
                $mdDialog.cancel();
            };
            $scope.answer = function (answer) {
                $mdDialog.hide(answer);
            };
            $scope.createCampaign = function () {

                console.log(JSON.stringify($scope.createCampaignRequest));
                driverHttpService.registerCampaign($scope.createCampaignRequest)
                    //.then(function (response) {
                    //  var campaignId = response.headers("location").split("/").pop();
                    //  console.log("response: " + response);
                    //});
                    .success(function (data, status, headers, config) {
                        console.log(data);
                        console.log(status);
                        console.log(headers());
                        console.log(config);

                        $mdDialog.hide();
                        $rootScope.$broadcast('updateCampaignsList');
                        //var driverId = headers("location").split("/").pop();
                        //$location.path('/detaleKierowcy/' + driverId);

                    })
                    .error(function (data, status, headers, config) {
                        console.log(data);
                        console.log(status);
                        console.log(headers());
                        console.log(config);
                    });
            };
        };

        function showAdd(ev) {
            $mdDialog.show({
                    controller: DialogController,
                    templateUrl: 'src/adroads.ui/view/createCampaign.html',
                    targetEvent: ev
                })
                .then(function (answer) {
                    $scope.alert = 'You said the information was "' + answer + '".';
                }, function () {
                    $scope.alert = 'You cancelled the dialog.';
                });
        }
    });


})();
