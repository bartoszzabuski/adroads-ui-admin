(function () {

    angular
        .module('users')
        .controller('MainController', [
            '$scope', 'userService', '$mdSidenav', '$mdBottomSheet', '$log', '$q', '$rootScope', 'LoggedInUserService', '$state', '$mdDialog',
            MainController
        ]);

    /**
     * Main Controller for the Angular Material Starter App
     * @param $scope
     * @param $mdSidenav
     * @param avatarsService
     * @constructor
     */
    function MainController($scope, userService, $mdSidenav, $mdBottomSheet, $log, $q, $rootScope, LoggedInUserService, $state, $mdDialog) {
        var self = this;

        self.loggedInUser = null;
        self.selected = null;
        self.users = [];
        self.selectUser = selectUser;
        self.toggleList = toggleUsersList;
        self.showContactOptions = showContactOptions;
        self.logout = logout;

        $rootScope.$on('authorized', function () {
            self.loggedInUser = LoggedInUserService.getLoggedIn();
        });

        $rootScope.$on('unauthorized', function () {
            self.loggedInUser = null;
            $state.go('login');
        });

        $rootScope.$on('error', function(event, msg){
           showAlert(msg);
        });

        function logout() {
            self.loggedInUser = null;
            LoggedInUserService.logOut();
            $state.go('login');
        }

        // Menu items
        $scope.menu = [
            {
                state: 'campaigns',
                title: 'Campaigns',
                icon: 'event_available'
            },
            {
                state: 'drivers',
                title: 'Drivers',
                icon: 'directions_car'
            },
            {
                state: 'register-driver',
                title: 'Zarejestruj kierowcę',
                icon: 'add'
            }
            //,
            //{
            //    link: '/campaigns-calendar',
            //    title: 'Campaigns calendar',
            //    icon: 'event'
            //}
        ];

        function showAlert(msg) {
            $mdDialog.show(
                $mdDialog.alert()
                    //.parent(angular.element(document.querySelector('#popupContainer')))
                    .clickOutsideToClose(true)
                    .title('Upssss....')
                    .textContent(msg)
                    .ariaLabel('Wystapil blad')
                    .ok('ok')
                    //.targetEvent(ev)
            );
        };

        // Load all registered users

        userService
            .loadAllUsers()
            .then(function (users) {
                self.users = [].concat(users);
                self.selected = users[0];
            });

        // *********************************
        // Internal methods
        // *********************************


        // Sidenav toggle
        function toggleSidenav(menuId) {
            console.log("toggleSidenav click")
            $mdSidenav(menuId).toggle().then(function () {
                console.log("toggle " + navID + " is done");
            });
            ;
        };

        /**
         * First hide the bottomsheet IF visible, then
         * hide or Show the 'left' sideNav area
         */
        function toggleUsersList() {
            var pending = $mdBottomSheet.hide() || $q.when(true);

            pending.then(function () {
                $mdSidenav('left').toggle();
            });
        }

        /**
         * Select the current avatars
         * @param menuId
         */
        function selectUser(user) {
            self.selected = angular.isNumber(user) ? $scope.users[user] : user;
            self.toggleList();
        }

        /**
         * Show the bottom sheet
         */
        function showContactOptions($event) {
            var user = self.selected;

            return $mdBottomSheet.show({
                parent: angular.element(document.getElementById('content')),
                templateUrl: '/view/contactSheet.html',
                controller: ['$mdBottomSheet', ContactPanelController],
                controllerAs: "cp",
                bindToController: true,
                targetEvent: $event
            }).then(function (clickedItem) {
                clickedItem && $log.debug(clickedItem.name + ' clicked!');
            });

            /**
             * Bottom Sheet controller for the Avatar Actions
             */
            function ContactPanelController($mdBottomSheet) {
                this.user = user;
                this.actions = [
                    {name: 'Phone', icon: 'phone', icon_url: 'assets/svg/phone.svg'},
                    {name: 'Twitter', icon: 'twitter', icon_url: 'assets/svg/twitter.svg'},
                    {name: 'Google+', icon: 'google_plus', icon_url: 'assets/svg/google_plus.svg'},
                    {name: 'Hangout', icon: 'hangouts', icon_url: 'assets/svg/hangouts.svg'}
                ];
                this.submitContact = function (action) {
                    $mdBottomSheet.hide(action);
                };
            }
        }

    }

})();
