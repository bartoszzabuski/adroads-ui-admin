(function () {

    angular
        .module('users').controller('DriversRegistrationController', function ($scope, driverHttpService, $mdDialog, $filter, carDetailsFactory) {
            var self = this;

            // ===== autocomplete for makes and models ==============

            self.selectedCarMake = null;
            self.selectedCarModel = null;
            self.searchCarMake = null;
            self.searchCarModel = null;
            self.dateOfBirth = null;

            self.queryMakes = queryMakes;
            self.queryModels = queryModels;

            carDetailsFactory.getMakes(function (response) {
                $scope.carMakes = response.data.map(function (make) {
                    return {
                        value: make.toLowerCase(),
                        display: make
                    };
                });
            });

            $scope.$watch("controller.selectedCarMake", function (make) {
                if (make) {
                    console.log("controller.selectedCarMake changed to " + make);
                    loadModels(make)
                }
            }, true);

            function loadModels(make){
                carDetailsFactory.getModels(make, function(response){
                    console.log('makes: ' + response.data);
                    $scope.carModels = response.data.map(function(x){
                        return {
                            value: x.toLowerCase(),
                            display: x
                        };
                    })
                })
            }

            function queryModels(query) {
                return query ? $scope.carModels.filter(createFilterFor(query)) : [];
            }

            function queryMakes(query) {
                return query ? $scope.carMakes.filter(createFilterFor(query)) : [];
            }

            function createFilterFor(query) {
                var lowercaseQuery = angular.lowercase(query);
                return function filterFn(item) {
                    return (item.value.indexOf(lowercaseQuery) === 0);
                };
            }

            $scope.carYears = carDetailsFactory.years;

            $scope.carBodies = carDetailsFactory.bodies;

            $scope.carColours = carDetailsFactory.colours;

            $scope.provinces = [
                'dolnośląskie',
                'kujawsko-pomorskie',
                'lubelskie',
                'lubuskie',
                'łódzkie',
                'małopolskie',
                'mazowieckie',
                'opolskie',
                'podkarpackie',
                'podlaskie',
                'pomorskie',
                'śląskie',
                'świętokrzyskie',
                'warmińsko-mazurskie',
                'wielkopolskie',
                'zachodniopomorskie'
            ];

            $scope.driversTypes = [
                {label: 'Przedstawiciel/handlowiec', value: '1'},
                {label: 'Kurier/Transport', value: '2'},
                {label: 'Taksówkarz', value: '3'},
                {label: 'Dojazd z/do pracy', value: '4'},
                {label: 'inny', value: '5'}
            ];

            self.registerDriver = registerDriver;

            function registerDriver() {
                //imporve this hack -> responsible for formating date before sending out request
                $scope.createDriverRequest.carDetails.make = self.selectedCarMake ? self.selectedCarMake.value : self.searchCarMake;
                $scope.createDriverRequest.carDetails.model = self.selectedCarModel ? self.selectedCarModel.value : self.searchCarModel;

                $scope.createDriverRequest.dateOfBirth = formatDateOfBirth(self.dateOfBirth);
                console.log(JSON.stringify($scope.createDriverRequest));
                driverHttpService.registerDriver($scope.createDriverRequest)
                    .then(function (response) {
                        if (response.status == '201') {
                            $mdDialog.show(
                                $mdDialog.alert()
                                    //.parent(angular.element(document.querySelector('#popupContainer')))
                                    .clickOutsideToClose(true)
                                    .title('Info')
                                    .textContent('Kierowca został stworzony')
                                    .ok('ok!')
                                //.targetEvent(ev)
                            );
                        } else {
                            console.log(response);
                        }
                    });
            }

            $scope.createDriverRequest = {};

            // TODO extract -> same bit in EditDriverController
            function formatDateOfBirth(date) {
                return $filter('date')(date, "dd-MM-yyyy");
            }

        }
    );

})();
